let btn = document.createElement('div');
btn.id = 'dbt_btn';
let darken = 'darken';
btn.textContent = darken;
btn.addEventListener('click', (event) => {
    let isDark = document.body.classList.toggle('dark');
    event.currentTarget.textContent = isDark ? 'lighten' : darken;
});
document.body.appendChild(btn);
btn.dispatchEvent(new Event('click'));
