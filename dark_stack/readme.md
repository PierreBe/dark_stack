# dark_stack

## Goal

This project is a basic exercise for me to learn how to make a browser extension. It fulfills a simple need : put dark mode in stackoverflow, even if I don't have an account.

## Structure

There are two ways to handle the problem.

One is very basic and consists of a bookmark executing javascript (bookmarklet.html). The drawback is that you have to manually click the bookmark to execute the code.

The second is the cleanest way to do the work. You have to set up a real extension (simpler that it sounds) and install it on your browser. With this approach, dark mode will automatically trigger when you open a stackoverflow tab.

## Useful links

- [The Coding Train - programming with text #11](https://thecodingtrain.com/learning/programming-with-text/11.1-introduction.html)
- [MDN - Build a cross browser extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Build_a_cross_browser_extension)
- [MDN - manifest.json](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json)
- [random stackoverflow page for testing](https://stackoverflow.com/help/how-to-ask)

## Tips

- Omnibox for mozilla extensions : `about:debugging#/runtime/this-firefox`
- Omnibox for chrome extensions : `chrome://extensions/`
